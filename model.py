from tensorflow import keras


def get_model(path):
    model_loaded = keras.models.load_model(path)
    return model_loaded
