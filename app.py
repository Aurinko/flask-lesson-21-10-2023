from flask import Flask, render_template, request

from processing import process
from model import get_model

app = Flask(__name__)
nn = get_model("titanic_mlp")


@app.route('/', methods=["get", "post"])
def hello():
    message = ""
    if request.method == "POST":
        age = request.form.get("age")
        pclass = request.form.get("pclass")
        sex = request.form.get("sex")
        sibsp = request.form.get("sibsp")
        parch = request.form.get("parch")
        try:
            fare = float(request.form.get("fare"))
        except:
            fare = 0.0
            message = message + f"Fare записан некорректно! Значение по умолчанию {fare}. "

        person = list(map(float, [age, pclass, sex, sibsp, parch, fare]))

        model_prediction = process([person], nn)
        message = message + model_prediction

    return render_template("index.html", message=message)


app.run()
