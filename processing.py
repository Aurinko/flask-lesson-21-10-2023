import numpy as np


def process(person, model):
    person_pred = model.predict(person)
    return f"Человек выжил с вероятностью {np.round(person_pred[0][0] * 100, 1)}%"
